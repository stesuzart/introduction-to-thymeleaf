package com.javagirls.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
/*
  Exemplo de página dinâmica
 */
@Controller
@RequestMapping("/exemplo")
public class ExampleController {

    @GetMapping
    public String dinamicPage(Model model){

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dataFormatada = format.format(new Date());

        model.addAttribute("hora", dataFormatada);
        return "fragments/dinamicPage";
    }

}
