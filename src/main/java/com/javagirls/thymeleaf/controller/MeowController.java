package com.javagirls.thymeleaf.controller;

import com.javagirls.thymeleaf.domain.Gato;
import com.javagirls.thymeleaf.hardcode.Raca;
import com.javagirls.thymeleaf.repository.GatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = {"/app/meow", "/"})
public class MeowController {

    @Autowired
    private GatoRepository gatoRepository;

    @GetMapping
    public String home(Model model){
        model.addAttribute("racas", Raca.values());

        return "fragments/home";
    }

    @GetMapping("registro")
    public String registro(Model model){
        model.addAttribute("racas", Raca.values());

        return "fragments/registro";
    }

    @PostMapping("/novo")
    public String novo(Gato gatinho){

        gatoRepository.save(gatinho);
        return "fragments/finalizacao";
    }

    @GetMapping("/lista")
    public String visualizar(Model model){

        List<Gato> gatos = gatoRepository.findAll();
        model.addAttribute("gatos", gatos);

        return "fragments/lista";
    }
}
