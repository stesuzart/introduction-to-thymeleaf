package com.javagirls.thymeleaf.domain;

import com.javagirls.thymeleaf.hardcode.Raca;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "GATO")
@Inheritance(strategy = InheritanceType.JOINED)
public class Gato {

    @Id
    @GenericGenerator(name = "gatoSequenceGenerator", parameters = @Parameter(name = "sequence_name", value = "GATO_SEQUENCE"), strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator")
    @GeneratedValue(generator = "gatoSequenceGenerator")
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "COR")
    private String cor;

    @Column(name = "RACA")
    @Enumerated(EnumType.STRING)
    private Raca raca;

    @Column(name = "SEXO")
    private char sexo;

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public Raca getRaca() {
        return raca;
    }

    public void setRaca(Raca raca) {
        this.raca = raca;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gato)) return false;
        Gato gato = (Gato) o;
        return Objects.equals(getId(), gato.getId());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Gato{");
        sb.append("id=").append(id);
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", cor='").append(cor).append('\'');
        sb.append(", raca=").append(raca);
        sb.append(", sexo=").append(sexo);
        sb.append('}');
        return sb.toString();
    }
}
