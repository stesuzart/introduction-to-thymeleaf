package com.javagirls.thymeleaf.repository;

import com.javagirls.thymeleaf.domain.Gato;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GatoRepository extends JpaRepository<Gato, Long> {
}
