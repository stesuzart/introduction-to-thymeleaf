package com.javagirls.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntroductionThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntroductionThymeleafApplication.class, args);
	}
}
