package com.javagirls.thymeleaf.hardcode;

public enum Raca {
    PERSA("Persa"),
    SIAMES("Siamês"),
    RACA_INDEFENIDA("Raça Indefinida");

    private String descricao;

    Raca(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}

