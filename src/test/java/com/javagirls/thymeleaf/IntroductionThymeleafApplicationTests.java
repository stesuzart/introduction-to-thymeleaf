package com.javagirls.thymeleaf;

import com.javagirls.thymeleaf.domain.Gato;
import com.javagirls.thymeleaf.hardcode.Raca;
import com.javagirls.thymeleaf.repository.GatoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IntroductionThymeleafApplicationTests {
	
	@Mock
	private GatoRepository gatoRepository;
	
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAll() {
		when(gatoRepository.findAll()).then(InvocationOnMock::getMock);
	}
	
	@Test
	public void tesRegister(){
		Gato gato = new Gato();
		gato.setCor("Branco");
		gato.setNome("Mestre Karin");
		gato.setRaca(Raca.PERSA);
		gato.setSexo('M');
		
		when(gatoRepository.save(gato)).thenReturn(new Gato());
		
	}

}
