<h1>Introduction to Thymeleaf</h1>

This project was developed to teach the basics of Thymeleaf with Spring Boot to the <a href="https://www.meetup.com/Devs-Java-Girl/events/254189486/">Devs Java Girl Community</a>.<br>
It was a great experience to lecture, to pass and to acquire a little knowledge about this subject.

<b><a href="https://docs.google.com/presentation/d/1pFwBReO7Fn1VT1d-prRrn2Hjjx1sk9Ai9M0bRT5XGvQ/edit?usp=sharing">Link</a></b> to the slide.

<h2>Getting Started</h2>
This project contains a form for registration of cats and a list of all the registers.

<h2>Prerequisites</h2>
<ul>
  <li><a href="https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html">Java 8</a></li>
  <li><a href="https://maven.apache.org">Maven</a></li>
  <li><a href="http://suite.opengeo.org/docs/latest/dataadmin/pgGettingStarted/pgadmin.html">PostgreSQL</a></li>
  <li><a href="http://suite.opengeo.org/docs/latest/dataadmin/pgGettingStarted/pgadmin.html">PGAdmin</a></li>
</ul>

<h2>Installing</h2>
<p>Follow the steps bellow to configure the project: </p>
1º Create a new Database with name <b>devjavagirls</b><br>
2º Create a new Schema with name <b>thymeleaf</b><br>
3º Run the project
